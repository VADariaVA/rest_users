var express = require('express');
var router = express.Router();

//GET /users
router.get('/users', (req, res) => {
	var db = req.db;
	var collection = db.get('users');
	collection.find({},{},(e,doc) => {
		res.json(doc);
	});
});

//POST /users
router.post('/users', (req, res) => {
	var db = req.db;
	var name = req.body.name;
	var id = req.body.id;
	var collection = db.get('users');
	collection.insert({
		"name" : name,
		"id" : id
	}, (err, doc) => {
		if (err) {
			res.send(500);
		}   res.send("Added user " + name + " with id "+ uid);
	});
});

//GET /users/:id
router.get('/users/:id', (req, res) => {
	var db = req.db;
	var id = req.params.id;
	var collection = db.get('users');
	collection.find({
		"id" : id
	}, {} ,(e,doc) => {
		res.json(doc);
	});
});

//PATCH /users/:id
//curl --request PATCH -d "name= "  http://localhost:3000/users/idishere
router.patch('/users/:id', (req, res) => {
	var id = req.params.id;
	var name = req.body.name;
	var db = req.db;
	var collection = db.get('users');
	collection.update({ "id" : id }, { $set: { "name" : name } });
});

//DELETE /users/:id
router.get('/users/:id', (req, res) => { 
	var db = req.db;
	var id = req.params.id;
	var collection = db.get('users');
	collection.remove({ "id" : id },{}, (e, doc) => {
		res.json(doc);
	});
});
module.exports = router;
